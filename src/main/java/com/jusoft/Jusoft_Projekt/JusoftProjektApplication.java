package com.jusoft.Jusoft_Projekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JusoftProjektApplication {

	public static void main(String[] args) {
		SpringApplication.run(JusoftProjektApplication.class, args);
	}

}
